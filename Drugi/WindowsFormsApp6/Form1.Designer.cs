﻿namespace WindowsFormsApp6
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.b2 = new System.Windows.Forms.Button();
            this.b1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.l2 = new System.Windows.Forms.Label();
            this.l1 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lb1_Rijec = new System.Windows.Forms.ListBox();
            this.lb1_Rez = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // b2
            // 
            this.b2.Location = new System.Drawing.Point(44, 380);
            this.b2.Name = "b2";
            this.b2.Size = new System.Drawing.Size(106, 47);
            this.b2.TabIndex = 0;
            this.b2.Text = "Izlaz";
            this.b2.UseVisualStyleBackColor = true;
            this.b2.Click += new System.EventHandler(this.b2_Click);
            // 
            // b1
            // 
            this.b1.Location = new System.Drawing.Point(44, 292);
            this.b1.Name = "b1";
            this.b1.Size = new System.Drawing.Size(106, 47);
            this.b1.TabIndex = 1;
            this.b1.Text = "Unesi";
            this.b1.UseVisualStyleBackColor = true;
            this.b1.Click += new System.EventHandler(this.b1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(31, 51);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 2;
            // 
            // l2
            // 
            this.l2.AutoSize = true;
            this.l2.Location = new System.Drawing.Point(41, 178);
            this.l2.Name = "l2";
            this.l2.Size = new System.Drawing.Size(66, 13);
            this.l2.TabIndex = 3;
            this.l2.Text = "Promaseno: ";
            // 
            // l1
            // 
            this.l1.AutoSize = true;
            this.l1.Location = new System.Drawing.Point(41, 141);
            this.l1.Name = "l1";
            this.l1.Size = new System.Drawing.Size(62, 13);
            this.l1.TabIndex = 4;
            this.l1.Text = "Pogodeno: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 215);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Pokusaji: ";
            // 
            // lb1_Rijec
            // 
            this.lb1_Rijec.FormattingEnabled = true;
            this.lb1_Rijec.Location = new System.Drawing.Point(137, 21);
            this.lb1_Rijec.Name = "lb1_Rijec";
            this.lb1_Rijec.Size = new System.Drawing.Size(120, 95);
            this.lb1_Rijec.TabIndex = 6;
            // 
            // lb1_Rez
            // 
            this.lb1_Rez.FormattingEnabled = true;
            this.lb1_Rez.Location = new System.Drawing.Point(137, 141);
            this.lb1_Rez.Name = "lb1_Rez";
            this.lb1_Rez.Size = new System.Drawing.Size(120, 95);
            this.lb1_Rez.TabIndex = 7;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(266, 450);
            this.Controls.Add(this.lb1_Rez);
            this.Controls.Add(this.lb1_Rijec);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.l1);
            this.Controls.Add(this.l2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.b1);
            this.Controls.Add(this.b2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button b2;
        private System.Windows.Forms.Button b1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label l2;
        private System.Windows.Forms.Label l1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox lb1_Rijec;
        private System.Windows.Forms.ListBox lb1_Rez;
    }
}

