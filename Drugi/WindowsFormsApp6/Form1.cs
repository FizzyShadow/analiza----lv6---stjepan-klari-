﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp6
{
    public partial class Form1 : Form
    {
        Random rand = new Random();
        List<string> list = new List<string>();
        int brojPokusaja;
        string RijecIzListe, RijecULabelu;
        string path = "C:\\Users\\Rerna\\Desktop\\txt.txt";
        public Form1()
        {
            InitializeComponent();
        }

        public void Reset()
        {
            RijecIzListe = list[rand.Next(0, list.Count - 1)];
            RijecULabelu = new string('*', RijecIzListe.Length);
            lb1_Rijec.Text = RijecULabelu;
            brojPokusaja = 5;
            lb1_Rez.Text = brojPokusaja.ToString();
        }

        private void b2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void b1_Click(object sender, EventArgs e)
        {
            if (b1.Text.Length == 1)
            {
                if (RijecIzListe.Contains(b1.Text))
                {
                    string temp_rijecx = RijecIzListe;
                    while (temp_rijecx.Contains(b1.Text))
                    {
                        int index = temp_rijecx.IndexOf(b1.Text);
                        StringBuilder builder = new StringBuilder

                        (temp_rijecx);

                        builder[index] = '*';
                        temp_rijecx = builder.ToString();
                        StringBuilder builder2 = new StringBuilder

                        (RijecULabelu);

                        builder2[index] = Convert.ToChar(b1.Text);
                        RijecULabelu = builder2.ToString();
                    }
                    b1.Text = RijecULabelu;
                    if (RijecULabelu == RijecIzListe)
                    {
                        MessageBox.Show("Winnn!", "Pobjeda");
                        Reset();
                    }
                }
                else
                {
                    brojPokusaja--;
                    lb1_Rez.Text = brojPokusaja.ToString();
                    if (brojPokusaja <= 0)
                    {
                        MessageBox.Show("Loser!", "Kraj");
                        Reset();
                    }
                }
            }
            else if (b1.Text.Length > 1)
            {
                if (RijecIzListe == b1.Text)
                {
                    MessageBox.Show("Winnn!", "Pobjeda");
                    Reset();
                }
                else
                {
                    brojPokusaja--;
                    lb1_Rez.Text = brojPokusaja.ToString();
                    if (brojPokusaja <= 0)
                    {
                        MessageBox.Show("Loser!", "Kraj");
                        Reset();
                    }
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string line;
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))

            {
                while ((line = reader.ReadLine()) != null)
                {
                    list.Add(line);
                }
                Reset();
            }

        }
    }
}
